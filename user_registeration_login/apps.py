from django.apps import AppConfig


class UserRegisterationLoginConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'user_registeration_login'
