from django.core.management.base import BaseCommand
from smtpd import SMTPServer
import asyncore


class CustomSMTPServer(SMTPServer):
    def process_message(self, peer, mailfrom, rcpttos, data, **kwargs):
        # Handle the received email here
        print(f"Received email from: {mailfrom}")
        print(f"To: {rcpttos}")
        print(f"Data:\n{data}")


class Command(BaseCommand):
    help = 'Starts the custom SMTP server'

    def handle(self, *args, **options):
        server = CustomSMTPServer(('localhost', 1025), None)
        try:
            print('SMTP server started on localhost:1025')
            asyncore.loop()
        except KeyboardInterrupt:
            pass