FROM python:3.8.10

ENV PYTHONBUFFERED=1

WORKDIR /Code

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY . . 

RUN python manage.py migrate

EXPOSE 8000 1025

CMD python manage.py runserver 0.0.0.0:8000 & python manage.py start_smtp_server