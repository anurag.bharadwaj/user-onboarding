this is an task performed for creating user_registeration_login interface.

follow below steps to setup the application in your environment:


step 1 : In your local machine or any machine on your cloud platform first we need to install python ,

        -> if you are using any linux distribution such as ubuntu, python comes pre-installed in it,

        -> for other system such as windows you can install python by downloding and following the steps mentioned in official site.

        -> you will need python package manager , here we are using pip. :-  "python -m ensurepip --upgrade".


step 2 : (this step is optional as you can set up this project on local root itself but its recommended to use virtual env), 

        -> create a folder in system name it as you want , and inside that folder run this command to createa virtual env  :-  
            
            python -m venv <virtual-env-name>

        -> next step is to activate the virtual env, for linux  :-  "python -m venv <virtual-env-name>" and to activate "source/<virtual-env-name>/activate
                                                    for  windows  :-  1. cd <virtual-env-name>/scripts
                                                                    2. activate

        ** the above steps will create and activate the virtual env for your development **


step 3 : now we will install django in the virtual env, at the time of developing this poc we were using "Django 4.2.1" 

        -> pip install Django==4.2.1

        ** this will install django in your virtual env**


step 4 : now you need to clone the actual project from git repo whose link you can get from tab called "code" in git interface and selecting the http option and copy it.

        -> then, in the folder you just created at same level as virtual env folder run  :-  git clone <-- http link to repo -->


step 5 : now once you clone the repo you will see that there is a requirements.txt file there , it contains all the required packages for this application to run with their verion which is used at the time of developing this app,

        -> simply run  :-  pip install -r requirements.txt

        ** this will install all the required packages for this app, it is an convenient way to install all dependencis **


step 6 : this app is using an python inbuilt package called "smtpd" to setup an local smtp server to moniter emails which are sent and recieved in context of sender and reciever, you can call it an simulation for the same,

        -> in the app folder which is "user_registeration_login" you will see that there is folder path called "management/commands" inside this there is "start_smtp_server.py" file , this is basically the additional command added to django application to run the local smtp server.

        -> once you start this server by running  :-  python manage.py start_smtp_server
        you will be able to moniter all actvity regarding sending and getting mails through this server.

        -> in this app we are using non-existant email addresses such as TestSender@example.com, because we are using local smtp server in system using these we can simulate the sender and reciever behaivour.


            
step 7 : now lets see the steps to start the application, make sure you are at level of manage.py with virtual env activated in your terminal
    
    -> first we will start the local smtp server  :-  python manage.py start_smtp_server

    -> now from the manage.py script level open new terminal and run :- python manage.py makemigrations
                                                                then :- python manage.py migrate

    -> now start the application :- python manage.py runserver.


    ** the above steps will run this application , but follow the steps in order **.

    ** the application will behave in two parts :


 ** first it will ask for your email address which for example can "TestReciever@example.com" , then on clicking the Register/Login button you will redirect to otp_verification , if you are already a part of system/application you will be logged in , if you are new user then you will be taken to otp_verification page and otp will be shared with you which you can see it in terminal where locla server is running. **

